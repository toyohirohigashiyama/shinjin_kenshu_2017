#実行時に200501a.jmnを代入する
import sys
from pyknp import Jumanpp

jumanpp = Jumanpp()
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            if mrph.hinsi == "動詞":
                print(mrph.genkei)
        data = ""
