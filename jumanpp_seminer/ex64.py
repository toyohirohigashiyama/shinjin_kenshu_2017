#
import sys
from pyknp import Jumanpp
from collections import defaultdict

freq = defaultdict(int)
jumanpp = Jumanpp()
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            freq[mrph.genkei] += 1
        data = ""
print(sorted(freq.items(),key=lambda x:x[1], reverse = True))
