#実行時に200501a.jmnを代入する
import sys
from pyknp import Jumanpp

jumanpp = Jumanpp()
data = ""
a_sum = 0
j_sum = 0
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            a_sum += 1
            if mrph.hinsi == "動詞" or mrph.hinsi == "形容詞":
                j_sum += 1
        data = ""
print(j_sum/a_sum)
