#coding: utf-8
from pyknp import KNP

# インスタンスを作成
knp = KNP(jumanpp=True)

# 文を解析し、解析結果を Python の内部構造に変換して result に格納
result = knp.parse("望遠鏡で泳いでいる少女を見た。")

# 1.各文節へのアクセス: bnst_list()
for bnst in result.bnst_list():
    print("".join(mrph.midasi for mrph in bnst.mrph_list()), end=" ")
print("\n")
