from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            count = 0  #カウントを初期化する
            for mrph in bnst.mrph_list():  #名詞の数をカウントする
                if mrph.hinsi == "名詞":
                    count += 1
            if count >= 2:  #名詞の数が2以上なら、表示する
                print("".join(mrph.midasi for mrph in bnst.mrph_list()))
        data = ""
