#実行時に200501a.knpを代入する
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            i = 0
            j = 0
            s = ""
            for mrph in bnst.mrph_list():
                s += mrph.midasi
                if mrph.hinsi == "名詞":
                    i = 1
                if mrph.hinsi == "接尾辞":
                    j = 1
            if i == 1 and j == 1:
                print(s)
        data = ""
