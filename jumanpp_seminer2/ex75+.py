#実行時に200501a.knpを代入する
from pyknp import KNP 
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            parent = bnst.parent
            if parent is not None:
                child_rep = ""
                parent_rep = ""
                for mrph in bnst.mrph_list():
                    if mrph.hinsi == "動詞" or mrph.hinsi == "形容詞":
                        child_rep += mrph.repname + " "
                for mrph in parent.mrph_list():
                    if mrph.hinsi == "名詞":
                        parent_rep += mrph.repname + " "
                if child_rep != "" and parent_rep != "":
                    print(child_rep, "->", parent_rep, parent.bnst_id - bnst.bnst_id - 1)
        data = ""
