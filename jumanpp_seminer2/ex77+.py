#実行時に200501a.knpを代入する
from pyknp import KNP 
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        i = 0
        s = ""
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                if mrph.hinsi == "名詞":
                    if i == 2:
                        print(s + mrph.midasi)
                    if (bnst.parent_id - bnst.bnst_id) != 1:
                        s = mrph.midasi
                        i = 1
                    else:
                        i = 0
                elif mrph.midasi == "の" and i == 1:
                    s += mrph.midasi
                    i = 2
                else:
                    i = 0
        data = ""
