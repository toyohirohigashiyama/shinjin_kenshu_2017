#1
print("#1")
i = 1
while i <= 30:
    print(i, end=" ")
    i += 1
print("\n")

#2
print("#2")
for i in range(2, 31, 2):
    print(i, end=" ")
print("\n")

#3
print("#3")
x = 0
for i in range(2, 31, 2):
    x += i
print(x, "\n")

#4
print("#4")
i = 10
x = 1
while i > 0:
    x *= i
    i -= 1
print(x, "\n")

#5
print("#5")
for j in range(1, 10):
    for i in range(1, 10):
        print("{0:2d}".format(i*j), end=" ")
    print("\n")


#6
print("#6")
i = 1
while i <= 30:
    if i%15 == 0:
        print("FizzBuzz", end=", ")
    elif i%3 == 0:
        print("Fizz", end=", ")
    elif i%5 == 0:
        print("Buzz", end=", ")
    else:
        print(i, end=", ")
    i += 1
print("\n")

#7
print("#7")
i = 2
while i <= 1000:
    x = 1
    j = 2
    while j < i:
        x *= (i%j)
        j += 1
    if x != 0:
        print(i, end=", ")
    i += 1
print("\n")
