#e4
print('#e4')
a = 'Yakult'
b = 'Swallows'
c = a[0] + b[0]
print(c, '\n')

#e5
print('#e5')
e = 'stressed'
print(e[::-1], '\n')

#e6
print('#e6')
f = 'パタトクカシーー'
g = ''
for i in range(0,7,2):
    g += f[i]
print(g, '\n')

#e7
print('#e7')
h = 'パトカー'
k = 'タクシー'
l = ''
for i in range(4):
    l += h[i] + k[i]
print(l, '\n')

#e8
print('#e8')
m = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."
n = m.split()
o = ''
i = 1
for s in n:
    if i == 1 or 5 <= i <=9 or i == 15 or i == 16 or i == 19:
        o += s[0] + ' '
    else:
        o += s[0:2] + ' '
    i += 1
print(o, '\n')

