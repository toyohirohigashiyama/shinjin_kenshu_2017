#e9 文字種とその数の列挙
print('#e9')
string = 'yakultswallows'
freq = {}
for s in string:
    if s not in freq:
        freq[s] = 1
    else:
        freq[s] += 1
for key in freq:
    print('%s-->%s, ' % (key, freq[key]), end='')
print('\n')

#e10 dictionaryの使用
print('#e10')
foods = ['eggs', 'spam', 'spam', 'bacon']
freq = {}
for food in foods:
    for s in food:
        if s not in freq:
            freq[s] = 1
        else:
            freq[s] += 1
for key in freq:
    print('%s-->%s, ' % (key, freq[key]), end='')
print('\n')

#e11 暗号化と復号の実装(シーザー暗号)
print('#e11')

def rot(s, num):    #sをnumの数だけ文字をずらす関数
    if ord(s) >= ord('a') and ord(s) <= ord('z'):
        return chr(ord('a') + (ord(s) - ord('a') + num)%26)
    if ord(s) >= ord('A') and ord(s) <= ord('Z'):
        return chr(ord('A') + (ord(s) - ord('A') + num)%26)
    else:
        return s

def f_rot(lis1, lis2, num):    #リストの文字をnumの数だけずらしたものを表示する関数
    for inp in lis1:
        temp = ""
        for s in inp:
            c = rot(s, num)
            temp += c
        lis2.append(temp)
    for s in lis2:
        print(s, end=' ')
    print('\n')


import sys
inputs = []
for arg in sys.argv:    #argv[1]にずらす数、argv[2]以降に暗号化したい文を入力する
    inputs.append(arg)
num = int(inputs[1])
del inputs[0:2]         #不要なものを削除する
cipher = []             #暗号化された文を格納するリスト
original = []           #復号化された文を格納するリスト
f_rot(inputs, cipher, num)
f_rot(cipher, original, -num)
