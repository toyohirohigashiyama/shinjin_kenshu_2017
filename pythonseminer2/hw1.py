#1 3つの数字を受け取り、最大数を返す関数

def max_3(a, b, c):
    maxi = a
    if maxi < b:
        maxi = b
    if maxi < c:
        maxi = c
    return maxi

a = 2345
b = 2460
c = 9328
print(max_3(a, b, c))
