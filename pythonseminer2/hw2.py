#2 配列の最大数とその添え字を返す関数
def max_list(lis):
    maxi = lis[0]
    i = 0
    for num in lis:
        if maxi < num:
            maxi = num
            idx = i
        i += 1
    return idx, maxi

arr = [623, 744, 366, 745, 648]
print(max_list(arr))
