#4-1
def fib(n):
    if n == 1:
        return 1
    elif n == 0:
        return 0
    else:
        return fib(n-1) + fib(n-2)

import sys
n = int(sys.argv[1])
print(fib(n), '\n')
