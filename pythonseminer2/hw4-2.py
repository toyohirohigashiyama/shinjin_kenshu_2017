#4-2

import sys
n = int(sys.argv[1])
fibo = [0, 1]
if n == 0 or n == 1:
    print(fibo[n])
elif n > 1:
    for i in range(2, n+1):
        fibo.append(fibo[i-1] + fibo[i-2])
    print(fibo[n])
