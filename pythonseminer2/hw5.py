#5 ユークリッドの互除法により最大公約数を返す関数

def euclid(a, b):
    r = 1
    if a > b:
        x = a
        y = b
    else:
        x = b
        y = a
    while r != 0:
        r = x%y
        x = y
        y = r
    return x

a = 35
b = 62
print(euclid(a, b))
