#6
import sys
import math
import time

t0 = time.clock()

n = int(sys.argv[1])
lis = list(range(2, n+1))

for i in lis:
    if i > math.sqrt(n):
        break
    for j in lis:
        if j != i and j%i ==0:
            lis.remove(j)
print(lis)

t1 = time.clock()

print("dt="+str(t1-t0)+"[s]")

"""
このプログラムの実行時間dt=0.5289440000000001[s]であった。
一方、前回の課題では、実行時間dt=41.995991000000004[s]であった。
よって、圧倒的にエラトステネスの篩の方が早いことがわかる。
"""
