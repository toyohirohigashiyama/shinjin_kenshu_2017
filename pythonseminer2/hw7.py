#7

#バブルソート
def bubble_sort(lis):
    i = 1  #最初に弾かれないように
    while i != 0:
        i = 0
        for n in range(len(lis) - 1):
            if lis[n] > lis[n+1]:
                lis[n], lis[n+1] = lis[n+1], lis[n]
                i += 1
    return lis

#クイックソート
def quick_sort(lis):
    if len(lis) <= 1:
        return lis
    pivot = [lis[0]]
    high = []
    low = []
    for num in range(1, len(lis)):
        if lis[num] > pivot[0]:
            high.append(lis[num])
        else:
            low.append(lis[num])
    high = quick_sort(high)
    low = quick_sort(low)
    return low + pivot + high
    
#マージソート
def merge_sort(lis):
    if len(lis) <= 1:
        return lis
    former = []
    latter = []
    for num in range(int(len(lis)/2)):
        former.append(lis[num])
    for num in range(int(len(lis)/2), len(lis)):
        latter.append(lis[num])
    former = merge_sort(former)
    latter = merge_sort(latter)
    lis = []
    while len(former) != 0 and len(latter) != 0:
        if former[0] < latter[0]:
            lis.append(former.pop(0))
        else:
            lis.append(latter.pop(0))
    if len(former) != 0:
        lis.extend(former)
    elif len(latter) != 0:
        lis.extend(latter)
    return lis


#確認用試行
lis = [23, 64, 39, 60, 72, 38, 59, 56]
lis1 = lis
print('バブルソート結果, ', bubble_sort(lis1))
lis2 = lis
print('クイックソート結果, ', quick_sort(lis2))
lis3 = lis
print('マージソート結果, ', merge_sort(lis3))
