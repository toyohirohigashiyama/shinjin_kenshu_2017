#チェスのクイーン

#チェス盤上のm行n列のマス(m,n=1~8)を、リストsquareを用いてsquare[8*(m-1)+(n-1)](2行3列ならsquare[10])と表すことにする。また、条件より一行あたりにおけるクイーンは一つであることに留意する。

## ←デバッグ用の行

def queen(count, m=1):  #m行目
    for n in range(1, S+1):  #n列目
        print(m, n, square[S*(m-1)+(n-1)]) ##
        if square[S*(m-1)+(n-1)] >= 0:  #これ以前のクイーンの邪魔をしない
            lis.append(square[S*(m-1)+(n-1)])  #リストにクイーンの位置を書き込む
            print(lis)   ##
            for a in range(1, 9):  ##
                for b in range(1, 9):  ##
                    print(square[S*(a-1)+(b-1)], end=', ')  ##
                print('\n')  ##
            if m < S:  #探索が最終行まで到達していない時
                for num in range(1, S+1):  #設置したクイーンの縦横方向を使用不可にする
                    if square[S*(num-1)+(n-1)] >= 0:
                        square[S*(num-1)+(n-1)] = -m
                    if square[S*(m-1)+(num-1)] >= 0:
                        square[S*(m-1)+(num-1)] = -m
                i = m
                j = n
                k = n
                while i < S:  #設置したクイーンの斜め方向を使用不可にする
                    if j-1 >= 1 and square[S*((i-1)+1)+((j-1)-1)] >= 0:
                        square[S*((i-1)+1)+((j-1)-1)] = -m
                    if k+1 <= S and square[S*((i-1)+1)+((k+1)-1)] >= 0:
                            square[S*((i-1)+1)+((k+1)-1)] = -m
                    i += 1
                    j -= 1
                    k += 1
                count = queen(count, m+1)
                for num in range(S*(m-1), S**2):  #一段階復元して次に行く
                    if square[num] == -m:
                        square[num] = num
            elif m == S:  #探索が最終行に到達し、条件を満たす組み合わせが見つかった時
                count += 1
                print(lis, 'pair', count)
            lis.pop()  #組み合わせの最後の一つを消す
    return count

S = 8  #クイーンの数(マスの一辺の数)
square = list(range(64))  #マス目
lis = []  #クイーンの位置
count = 0  #組み合わせの数

print("組み合わせの数:", queen(count))
