import sys
import re
import gzip
from collections import defaultdict

def match_prob(strings, uni, bi):  #与えられた文章の現れる確率を求める。
    arr = strings.lower().split()  #与文章の分割
    bi_arr = []  #与文章のbigramの生成
    for (a, b) in zip(arr, arr[1:]):
        s = a + " " + b
        bi_arr.append(s)
    p = 1.0  #簡単のために、文頭に'The'がくる確率を1としました。
    for i in range(0, len(arr)-1):  #bigramによる確率の計算
        print(p)
        p *= (bi[bi_arr[i]])/(uni[arr[i]])
    return p

    
uni = defaultdict(int)
bi = defaultdict(int)

with gzip.open('/share/text/WWW.en/txt.en/tsubame00/doc0000000000.txt.gz', 'rt') as f:  #ファイルを開く
    for LINE in f:
        line = LINE.lower()
        if not (re.match(r"<page url=", line) or re.match(r"</page>", line)):  #必要ない行を省く
            words = line.split()
            for s in words:  #unigramを作る
                uni[s] += 1
            for (a, b) in zip(words, words[1:]):  #bigramを作る
                s = a + " " + b
                bi[s] += 1
strings = "The man is in the house."
print("最終的な確率:", match_prob(strings, uni, bi))
